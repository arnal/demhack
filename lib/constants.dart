import 'package:flutter/material.dart';

const primaryColor = Color(0xFFFE3434);
const appBarTextColor = Color(0xFF6A80D6);
const greyColor = Color(0xFFE5E5E5);
const grey1TextColor = Color(0xFF6D7275);
const greyIconColor = Color(0xFFCBCDCE);
const greyPhoneColor = Color(0xFFEAEBEB);
const primaryPressedColor = Color(0xFFE53030);
const secondaryColor = Color(0xFF19A6FF);
const secondaryPressedColor = Color(0xFF1698EB);
const lightBlueColor = Color(0xFFE8F5FD);
const greenColor = Color(0xFF24C451);
const errorColor = Color(0xFFF33B44);
const darkGrayColor = Color(0xFF2E353A);
const imageBackColor = Color(0xFFFFC4C4);
const popUpTextColor = Color(0xFF616161);
const backReserved = Color(0xFFF9F9F9);

const Map<int, Color> color = {
  50: Color.fromRGBO(254, 52, 52, .1),
  100: Color.fromRGBO(254, 52, 52, .2),
  200: Color.fromRGBO(254, 52, 52, .3),
  300: Color.fromRGBO(254, 52, 52, .4),
  400: Color.fromRGBO(254, 52, 52, .5),
  500: Color.fromRGBO(254, 52, 52, .6),
  600: Color.fromRGBO(254, 52, 52, .7),
  700: Color.fromRGBO(254, 52, 52, .8),
  800: Color.fromRGBO(254, 52, 52, .9),
  900: Color.fromRGBO(254, 52, 52, 1),
};
const MaterialColor kPrimaryColor = MaterialColor(0xFFFE3434, color);

const kDefaultPadding = 16.0;

const kGoogleApiKey = 'AIzaSyDZkJzeIJ-KhgLPt-h2hPV9xll4iV_T184';

const emailReg = '^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+.[a-z]';

const pattern = r'(^((\+380)+([0-9]){9})$)';
const pattern2 = r'(^((\+20)+([0-9]){10})$)';

String? validateMobile(String? value) {
  final regExp = RegExp(pattern);
  if (value!.isEmpty || !regExp.hasMatch(value)) {
    return 'Неправильно введенный формат номера телефона';
  }
  return null;
}

final styleButton = ElevatedButton.styleFrom(
  primary: Colors.transparent, // background
  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
  textStyle: const TextStyle(fontSize: 14, fontWeight: FontWeight.w700),
);

const txtHomeTitleStyle = TextStyle(
  fontSize: 18,
  fontWeight: FontWeight.w700,
  // color: kTextBlackColor2,
);

const authorizationTextStyle = TextStyle(
  fontSize: 20,
  fontWeight: FontWeight.w500,
  color: primaryColor,
);
const authorizationTitleTextStyle = TextStyle(
  fontSize: 20,
  fontWeight: FontWeight.w500,
  color: darkGrayColor,
);
const authorizationDescTextStyle = TextStyle(
  fontSize: 16,
  fontWeight: FontWeight.w500,
  color: grey1TextColor,
);
const registrationTextStyle = TextStyle(
  fontSize: 16,
  fontWeight: FontWeight.w400,
  color: grey1TextColor,
);
const patientDetailTextStyle = TextStyle(
  fontSize: 12,
  fontWeight: FontWeight.w400,
  color: grey1TextColor,
);

const registerTextStyle = TextStyle(
  fontWeight: FontWeight.w400,
  fontSize: 14,
  color: grey1TextColor,
);

const medicine1TextStyle = TextStyle(
  fontWeight: FontWeight.w400,
  fontSize: 14,
  color: darkGrayColor,
);

const medicineTextStyle = TextStyle(
  fontWeight: FontWeight.w400,
  fontSize: 12,
  color: grey1TextColor,
);

