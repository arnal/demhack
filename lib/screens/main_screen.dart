import 'package:demhack/constants.dart';
import 'package:demhack/screens/app_detail.dart';
import 'package:demhack/screens/personal_info.dart';
import 'package:device_apps/device_apps.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';

class MainScreen extends StatelessWidget {
  static const String id = '/main_screen';

  const MainScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: greyColor,
      appBar: AppBar(
        backgroundColor: Colors.white,
        centerTitle: true,
        shape: ContinuousRectangleBorder(
          borderRadius: const BorderRadius.only(
            bottomLeft: Radius.circular(50.0),
            bottomRight: Radius.circular(50.0),
          ),
        ),
        title: Text(
          'Анализ приложений',
          style: TextStyle(
            color: appBarTextColor,
          ),
        ),
      ),
      body:  FutureBuilder<List<Application>>(
        future: DeviceApps.getInstalledApplications(
            includeAppIcons: true,
            includeSystemApps: true,
            onlyAppsWithLaunchIntent: true),
        builder: (BuildContext context, AsyncSnapshot<List<Application>> data) {
          if (data.data == null) {
            return const Center(child: CircularProgressIndicator());
          } else {
            List<Application> apps = data.data!;

            return Scrollbar(
              child: ListView.builder(
                  itemBuilder: (BuildContext context, int position) {
                    Application app = apps[position];
                    return Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Card(
                        elevation: 8,
                        child: Column(
                          children: <Widget>[
                            ListTile(
                              leading: app is ApplicationWithIcon
                                  ? CircleAvatar(
                                backgroundImage: MemoryImage(app.icon),
                                backgroundColor: Colors.white,
                              )
                                  : null,
                              onTap: (){
                                Navigator.pushNamed(context, AppDetail.id, arguments: app);
                              },
                              //onTap: () => onAppClicked(context, app),
                              title: Text('${app.appName} (${app.packageName})'),
                              subtitle: Text('Version: ${app.versionName}\n'
                                  //'System app: ${app.systemApp}\n'
                                  //'APK file path: ${app.apkFilePath}\n'
                                  //'Data dir: ${app.dataDir}\n'
                                  //'Installed: ${DateTime.fromMillisecondsSinceEpoch(app.installTimeMillis).toString()}\n'
                              //    'Updated: ${DateTime.fromMillisecondsSinceEpoch(app.updateTimeMillis).toString()}'
                              ),
                            ),
                            const Divider(
                              height: 1.0,
                            )
                          ],
                        ),
                      ),
                    );
                  },
                  itemCount: apps.length),
            );
          }
        },
      )
    );
  }
  void onAppClicked(BuildContext context, Application app) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(app.appName),
            actions: <Widget>[
              _AppButtonAction(
                label: 'Open app',
                onPressed: () => app.openApp(),
              ),
              _AppButtonAction(
                label: 'Open app settings',
                onPressed: () => app.openSettingsScreen(),
              ),
            ],
          );
        });
  }
}
class _AppButtonAction extends StatelessWidget {
  final String label;
  final VoidCallback? onPressed;

  _AppButtonAction({required this.label, this.onPressed});

  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: () {
        onPressed?.call();
        Navigator.of(context).maybePop();
      },
      child: Text(label),
    );
  }
}
