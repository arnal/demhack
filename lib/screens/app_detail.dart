import 'package:device_apps/device_apps.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';

import '../constants.dart';

class AppDetail extends StatelessWidget {
  static const String id = '/app_detail_screen';
  late Application app;

  @override
  Widget build(BuildContext context) {
    app = ModalRoute.of(context)!.settings.arguments as Application;
    var style = TextStyle(
      fontSize: 20,
      color: Colors.blueGrey,
    );
    return Scaffold(
      backgroundColor: greyColor,
      appBar: AppBar(
        backgroundColor: Colors.white,
        centerTitle: true,
        shape: ContinuousRectangleBorder(
          borderRadius: const BorderRadius.only(
            bottomLeft: Radius.circular(50.0),
            bottomRight: Radius.circular(50.0),
          ),
        ),
        title: Text(
          app.appName,
          style: TextStyle(
            color: appBarTextColor,
          ),
        ),
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(18.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                '${app.appName} (${app.packageName})',
                style: style,
              ),
              SizedBox(height: 10),
              Text(
                'Version: ${app.versionName}\n'
                'System app: ${app.systemApp}\n'
                'APK file path: ${app.apkFilePath}\n'
                'Data dir: ${app.dataDir}\n'
                'Installed: ${DateTime.fromMillisecondsSinceEpoch(app.installTimeMillis).toString()}\n'
                'Updated: ${DateTime.fromMillisecondsSinceEpoch(app.updateTimeMillis).toString()}',
                style: style,
                textAlign: TextAlign.start,
              ),
              SizedBox(height: 20),
              Align(
                  alignment: Alignment.center,
                  child: ElevatedButton(
                      onPressed: () {
                        onAppClicked(context, app);
                      },
                      child: Text('Actions')))
            ],
          ),
        ),
      ),
    );
  }

  void onAppClicked(BuildContext context, Application app) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(app.appName),
            actions: <Widget>[
              _AppButtonAction(
                label: 'Open app',
                onPressed: () => app.openApp(),
              ),
              _AppButtonAction(
                label: 'Open app settings',
                onPressed: () => app.openSettingsScreen(),
              ),
            ],
          );
        });
  }
}

class _AppButtonAction extends StatelessWidget {
  final String label;
  final VoidCallback? onPressed;

  _AppButtonAction({required this.label, this.onPressed});

  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: () {
        onPressed?.call();
        Navigator.of(context).maybePop();
      },
      child: Text(label),
    );
  }
}
