import 'package:demhack/screens/app_detail.dart';
import 'package:demhack/screens/main_screen.dart';
import 'package:demhack/screens/personal_info.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'DEMHACK2021',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      initialRoute: MainScreen.id,
      routes: {
        MainScreen.id: (context) => MainScreen(),
        AppDetail.id: (context) => AppDetail(),
        PersonalInfo.id: (context) => PersonalInfo(),
      },
    );
  }
}
